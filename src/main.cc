#include <array>
#include <iostream>
#include <string>

#include "coolness-detector.h"

int main()
{
    std::cout << "Which participants are cool?" << std::endl;

    std::array<std::string, 7> MEMBERS
    {
        "pavel",
        "nathan",
        "chris",
        "andri",
        "taniguchi",
        "nagayama",
        "remco"
    };

    for (const auto& member : MEMBERS)
        if (isCool(member))
            std::cout << member << " is cool." << std::endl;

    return 0;
}
